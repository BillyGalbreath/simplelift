package net.pl3x.bukkit.simplelift;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class SimpleLift extends JavaPlugin implements Listener {
    private Set<UUID> cooldown = new HashSet<>();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onClickBlock(PlayerInteractEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) {
            return; // only listen to main hand packet
        }

        Player player = event.getPlayer();
        if (player.isSneaking()) {
            return; // allow for breaking the signs
        }

        if (cooldown.contains(player.getUniqueId())) {
            return; // on cooldown
        }

        Block signBlock = event.getClickedBlock();
        if (signBlock == null || !(signBlock.getState() instanceof Sign)) {
            return; // did not click a sign
        }

        String line0 = ((Sign) signBlock.getState()).getLine(0);
        if (line0 == null || !line0.equalsIgnoreCase("[lift]")) {
            return; // not a lift sign
        }

        World world = signBlock.getWorld();
        Location signLoc = signBlock.getLocation();
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            for (int y = signLoc.getBlockY() + 1; y < world.getMaxHeight(); y++) {
                if (activateLift(player, signLoc, y)) {
                    event.setCancelled(true);
                    return;
                }
            }
        } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            for (int y = signLoc.getBlockY() - 1; y > 0; y--) {
                if (activateLift(player, signLoc, y)) {
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    private boolean activateLift(Player player, Location signLoc, int y) {
        Block block = player.getWorld().getBlockAt(signLoc.getBlockX(), y, signLoc.getBlockZ());

        if (block == null || !(block.getState() instanceof Sign)) {
            return false; // not a sign
        }

        if (!((Sign) block.getState()).getLine(0).equalsIgnoreCase("[lift]")) {
            return false; // not a lift sign
        }

        Location playerLoc = player.getLocation();
        Block floor = player.getWorld().getBlockAt(playerLoc.getBlockX(), y - 2, playerLoc.getBlockZ());
        if (!floor.getType().isSolid()) {
            return false; // not a valid floor
        }

        Block destinationFeet = floor.getRelative(BlockFace.UP);
        if (destinationFeet.getType().isSolid()) {
            return false; // not a valid feet destination
        }

        Block destinationHead = destinationFeet.getRelative(BlockFace.UP);
        if (destinationHead.getType().isSolid() && !(destinationHead.getState() instanceof Sign)) {
            return false; // not a valid head location
        }

        Location destinationLoc = playerLoc.clone();
        destinationLoc.setY(y - 1);
        destinationLoc.setYaw(playerLoc.getYaw());
        destinationLoc.setPitch(playerLoc.getPitch());

        player.teleport(destinationLoc, PlayerTeleportEvent.TeleportCause.PLUGIN);

        UUID uuid = player.getUniqueId();
        cooldown.add(uuid);
        Bukkit.getScheduler().runTaskLater(this, () -> cooldown.remove(uuid), 10);
        return true;
    }
}
